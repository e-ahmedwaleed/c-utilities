# Samples

## String Builder

```c
#include <stdio.h>
#include "c-utilities/StringBuilder.h"

int main() {
	struct StringBuilder *string = new_StringBuilder();

    StringBuilder.append(string, "Hello, ");
    StringBuilder.append(string, "Mr. %s%d.\n", "user-", 1);
    printf("%s", StringBuilder.toString(string));

    free_StringBuilder(string);
    return 0;
}
```

`output: Hello, Mr. user-1.`

## Logger

```c
#include "c-utilities/Logger.h"

int main() {
    LOG_INFO("Main started.");

    LOG_ERROR("You can see me.");
    LOG_LEVEL(MUTE);
    LOG_ERROR("Now you don't.");

    return 0;
}
```

`output: ./cmake-build-debug/program.log`

## Instruments

```c
// include compiler flags [GCC_PROFILE_FUNCTIONS], check CMakeLists.txt
#include "c-utilities/Instruments.h"

void func() {return;}

int main() {
    INSTRUMENT_ADD(func);

    for (int i = 0; i < 10; i++)
        func();

    return 0;
}
```

`output: ./cmake-build-debug/traces/`

## CMakeLists.txt

```
set(PROJECT_NAME ...)
project(${PROJECT_NAME} C)

set(CMAKE_C_STANDARD 11)

add_executable(${PROJECT_NAME}
	c-utilities/Logger.h     c-utilities/StringBuilder.h      c-utilities/Instruments.h
	c-utilities/bin/Logger.o c-utilities/bin/StringBuilder.o  c-utilities/bin/Instruments.o
	main.c)

# include when using Instruments.h
set(GCC_PROFILE_FUNCTIONS "-Wl,--no-as-needed -ldl -finstrument-functions -DINSTRUMENTATION_ON")
set(CMAKE_C_FLAGS "${GCC_PROFILE_FUNCTIONS} ${CMAKE_C_FLAGS}")
```
