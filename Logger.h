//
// Created by Ahmed Waleed on 11/04/2020.
//

#ifndef UTILITY__LOGGER_H
#define UTILITY__LOGGER_H

// Changes the minimum level of details to be logged.
#define LOG_LEVEL(X) Log.level = X

// Writes formatted output to the logger (using printf style).
#define LOG_TRACE(...) Log.trace(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
// Writes formatted output to the logger (using printf style).
#define LOG_DEBUG(...) Log.debug(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
// Writes formatted output to the logger (using printf style).
#define LOG_INFO(...) Log.info(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
// Writes formatted output to the logger (using printf style).
#define LOG_WARNING(...) Log.warning(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
// Writes formatted output to the logger (using printf style).
#define LOG_ERROR(...) Log.error(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
// Writes formatted output to the logger (using printf style).
#define LOG_FATAL(...) Log.fatal(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

//https://www.tutorialspoint.com/log4j/log4j_logging_levels.htm
enum priority {
    // Designates finer-grained informational events than the DEBUG. (default value)
    TRACE,
    // Designates fine-grained informational events that are most useful to debug an application.
    DEBUG,
    // Designates informational messages that highlight the progress of the application at coarse-grained level.
    INFO,
    // Designates potentially harmful situations. (stdout)
    WARNING,
    // Designates error events that might still allow the application to continue running. (stderr)
    ERROR,
    // Designates very severe error events that will presumably lead the application to abort. (stderr)
    FATAL,
    // The highest possible rank and is intended to turn off logging.
    MUTE
};

struct Logger {
    enum priority level;

    void (*trace)(const char *file, const char *func, int line, const char *format, ...);

    void (*debug)(const char *file, const char *func, int line, const char *format, ...);

    void (*info)(const char *file, const char *func, int line, const char *format, ...);

    void (*warning)(const char *file, const char *func, int line, const char *format, ...);

    void (*error)(const char *file, const char *func, int line, const char *format, ...);

    void (*fatal)(const char *file, const char *func, int line, const char *format, ...);
} Log;

#endif //UTILITY__LOGGER_H
