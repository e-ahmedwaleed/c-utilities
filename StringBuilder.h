//
// Created by Ahmed Waleed on 11/19/2020.
//

#ifndef UTILITY__STRINGBUILDER_H
#define UTILITY__STRINGBUILDER_H

#include <stdbool.h>

struct StringNode;

struct StringBuilder {
    // Total string length stored in this StringBuilder.
    size_t length;
    // (DO NOT USE): Head node of the linked list storing the appended strings.
    struct StringNode *head;
    // (DO NOT USE): Tail node of the linked list storing the appended strings.
    struct StringNode *tail;

    // Appends given characters of a string to this StringBuilder. (using fprintf style)
    void (*append)(struct StringBuilder *this, char *format, ...);

    // Converts all the data in this StringBuilder to a single char[].
    char *(*toString)(struct StringBuilder *this);

} StringBuilder;

// Allocates memory for a new StringBuilder instance.
struct StringBuilder *new_StringBuilder();
// Frees memory allocated for a StringBuilder instance.
bool free_StringBuilder(struct StringBuilder *this);

#endif //UTILITY__STRINGBUILDER_H
