//
// Created by Ahmed Waleed on 11/22/2020.
//

//https://youtu.be/M6RCUiZzl8Y
#ifndef UTILITY__INSTRUMENTS_H
#define UTILITY__INSTRUMENTS_H

// Function attribute: can be used to reduce the number of instrumentation calls.
#define untraced __attribute__((no_instrument_function))

#ifndef INSTRUMENTATION_ON

// Function is not defined, please check your compiler flags! (@ CMakeLists.txt)
#define INSTRUMENT_ADD(func) NULL
// Function is not defined, please check your compiler flags! (@ CMakeLists.txt)
#define INSTRUMENT_REMOVE(func) NULL

#else

#include <stdbool.h>

// Adds function to the instrumentation table to trace and monitor its performance.
#define INSTRUMENT_ADD(func) traceFunction(func,#func)
// Removes function from the instrumentation table and deleting its data, e.a. untrace it.
#define INSTRUMENT_REMOVE(func) untraceFunction(func)

untraced bool traceFunction(void *func_ptr, char *func_name);

untraced bool untraceFunction(void *func_ptr);

#endif //INSTRUMENTATION_ON

#endif //UTILITY__INSTRUMENTS_H
